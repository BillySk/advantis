import React from 'react';
import './MainPanel.css';
import { desiredHeights } from '../constants/DesiredHeights';

class MainPanel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            imageDragged: this.props.imageDragged,
            imageViewer: [],
            imageStyles: {},
            timesScrolledDown: 0
        }
        this.scroll = this.scroll.bind(this);
    }

    scroll (e, width, height) {
        let count = this.state.timesScrolledDown;
        const rows = 8;
        const columns = 9;
        if (e.deltaY > 0 && count < (rows*columns-2)) {
            count++;
        } else if (e.deltaY < 0 && count > 0){
            count--;
        }
        let currentY = Math.trunc(this.state.timesScrolledDown / columns);
        for(var j=0; j < document.getElementsByClassName(`imageViewer${this.props.index}`).length; j++) {
            document.getElementsByClassName(`imageViewer${this.props.index}`)[j].style.backgroundPosition = `${-this.state.timesScrolledDown % columns * width/height * desiredHeights.mainPanel}px ${-currentY * desiredHeights.mainPanel}px`;
        }
        this.setState({timesScrolledDown: count});
    }

    render() {
        const {imageDragged} = this.props;
        const {width, height} = imageDragged?.dimensions || {width: 0, height: 0};
        const rows = 8;
        const columns = 9;
        let currentY = Math.trunc(this.state.timesScrolledDown / columns);
        let currentX = this.state.timesScrolledDown % columns;
        const imageStyles = {
            width: width && height ? `${width/height * desiredHeights.mainPanel}px` : '0px',
            height: width && height ? `${desiredHeights.mainPanel}px` : '0px',
            backgroundImage: `url(${imageDragged?.url || null})`,
            backgroundSize: `${desiredHeights.mainPanel * (width/height) * columns}px ${desiredHeights.mainPanel * rows}px`,
            backgroundPosition: `${- currentX* width/height * desiredHeights.mainPanel}px ${-currentY*desiredHeights.mainPanel}px`,
            backgroundColor: 'black',
            opacity: 1 - ((this.props.numberOfDraggedImages -1) * 0.25) 
        };

        for(var i=this.state.imageViewer.length; i < this.props.numberOfDraggedImages; i++) {
            this.state.imageViewer.push(<div className={`imageViewer${this.props.index}`}
                                            onWheel = {(e) => this.scroll(e, width, height)}
                                            key={this.state.imageViewer.length}
                                            // src={imageDragged?.url || null}
                                            style={imageStyles}
                                        />);
        }

        return(
            <div className={`mainPanel${this.props.index}`}>
                {this.state.imageViewer}
            </div>
        )
    }
}
export default MainPanel;
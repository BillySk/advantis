import React from 'react';
import './LeftPanel.css';
import { images } from '../constants/Images';
import RowContainer from './rowContainer/RowContainer';

class LeftPanel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            images: [],
            allImages: images,
            numberOfDraggedImages: 0,
            showCollapseButton: true,
            showPanel: true
        }
        this.passDragged = this.passDragged.bind(this);
        this.setNumberOfDraggedImages = this.setNumberOfDraggedImages.bind(this);
        this.toggleShowPanel = this.toggleShowPanel.bind(this);
    }

    passDragged(image) {
        this.props.passDragged(image)
    }

    setNumberOfDraggedImages(index) {
        this.props.setNumberOfDraggedImages(index);
    }

    toggleShowPanel() {
        this.setState({
            showCollapseButton: !this.state.showCollapseButton,
            showPanel: !this.state.showPanel
        });
    }    

    render() {
        return(
            <div className={this.state.showPanel ? "leftPanel" : "leftPanel hide"}>
                <div className={this.state.showCollapseButton ? "collapseButton" : "uncollapseButton"} onClick={this.toggleShowPanel}></div>
                {this.state.allImages.map((image) => {
                    return (
                    <div key={image.name}>
                        <RowContainer image={image} passDragged={this.passDragged} setNumberOfDraggedImages={this.setNumberOfDraggedImages}/>
                    </div>
                    )
                })
                }
            </div>
        )
    }
}
export default LeftPanel;
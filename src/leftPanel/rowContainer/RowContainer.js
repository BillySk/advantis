import React from 'react';
import './RowContainer.css';
import {DraggableCore}  from 'react-draggable';
import { desiredHeights } from '../../constants/DesiredHeights';

class RowContainer  extends React.Component {
    constructor(props) {
         super(props);
         this.state = {
            dimensions: {},
            deltaPosition: {x: 0,y:0},
        };
         this.handleDrag = this.handleDrag.bind(this);
         this.handleStop = this.handleStop.bind(this);
         this.getDimensions = this.getDimensions.bind(this);
    }


    componentDidMount() {
        let self = this;
        this.getDimensions(
            this.props.image.url,
            function(width, height) { self.setState({dimensions: {width: width, height: height}}) }
          );
    }

    getDimensions(url, callback) {
        var img = new Image();
        img.src = url;
        img.onload = function() { callback(this.width, this.height); }
    }

    handleDrag(e, ui) {
        const { x, y } = this.state.deltaPosition;
        
        this.setState({
            deltaPosition: {
              x: x + ui.deltaX,
              y: y + ui.deltaY,
            }
        });
    }

    handleStop(ev) {
        if(ev.target.className.indexOf('mainPanel') !== -1 || ev.target.className.indexOf('imageViewer') !== -1 ) {
            let tempImage = {...this.props.image, ...{['dimensions']: this.state.dimensions}};
            this.props.passDragged(tempImage);
            let index = null;
            if (ev.target.className.indexOf('mainPanel') !== -1) {
                index = ev.target.className.replace('mainPanel', '')
            } else {
                index = ev.target.className.replace('imageViewer', '')
            }
            this.props.setNumberOfDraggedImages(index);
        }
    }


    render() {
        const {image} = this.props;
        const {width, height} = this.state.dimensions;
        const rows = 8;
        const columns = 9;
        const imageStyles = {
            width: `${width/height * desiredHeights.leftContainer}px`,
            height: `${desiredHeights.leftContainer}px`,
            //we demonstrate the element at 4th row and 4th column
            backgroundPosition: `${4 * width/height * desiredHeights.leftContainer}px ${4 * desiredHeights.leftContainer}px`,
            backgroundImage: `url(${image.url})`,
            backgroundSize: `${desiredHeights.leftContainer*(width/height)*columns}px ${desiredHeights.leftContainer*rows}px`
        }
        return (
            <DraggableCore
                onDrag={this.handleDrag}
                onStop={this.handleStop}
                draggable="true"
            >
                <div className="rowContainer">
                    <div className="name">{image.name}</div>
                    <div src={image.url} style={imageStyles} className="divImage"/>
                </div>
            </DraggableCore>
        );
    }
}

export default RowContainer;
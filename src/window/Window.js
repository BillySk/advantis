import React from 'react';
import './Window.css';
import LeftPanel from '../leftPanel/LeftPanel';
import MainPanel from '../mainPanel/MainPanel';

class Window extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            imageDragged: null,
            numberOfDraggedImages: [0,0,0,0]
        }
        this.passDragged = this.passDragged.bind(this);
        this.setNumberOfDraggedImages = this.setNumberOfDraggedImages.bind(this);
    }

    passDragged(image) {
        this.setState({imageDragged: image})
    }

    setNumberOfDraggedImages(index) {
        let temp = this.state.numberOfDraggedImages;
        temp[index]++;
        this.setState({numberOfDraggedImages: temp})
    }

    render() {
        return(
            <div className="window">
                <LeftPanel passDragged={this.passDragged} setNumberOfDraggedImages={this.setNumberOfDraggedImages} numberOfDraggedImages={this.state.numberOfDraggedImages}/>
                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <MainPanel imageDragged={this.state.imageDragged} numberOfDraggedImages={this.state.numberOfDraggedImages[0]} index={0}/>
                            </td>
                            <td>
                                <MainPanel imageDragged={this.state.imageDragged} numberOfDraggedImages={this.state.numberOfDraggedImages[1]} index={1}/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <MainPanel imageDragged={this.state.imageDragged} numberOfDraggedImages={this.state.numberOfDraggedImages[2]} index={2}/>
                            </td>
                            <td>
                                <MainPanel imageDragged={this.state.imageDragged} numberOfDraggedImages={this.state.numberOfDraggedImages[3]} index={3}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}
export default Window;
export const images = [
    {
        name: "T1",
        url: "https://s3-eu-central-1.amazonaws.com/advantis-public/t1.png"
    },
    {
        name: "DWI",
        url: "https://s3-eu-central-1.amazonaws.com/advantis-public/dwi.png"
    },
    {
        name: "ColorMap",
        url: "https://s3-eu-central-1.amazonaws.com/advantis-public/colormap.png"
    },
]
import React from 'react';
import { shallow, mount } from 'enzyme';
import App from '../../../src/App';
import Window from '../../window/Window';
import LeftPanel from '../../leftPanel/LeftPanel';
import MainPanel from '../../mainPanel/MainPanel';

describe('App', () => {
    let wrapper;
    let wrapper2;
  
    beforeEach(() => wrapper = shallow(<App />));
    beforeEach(() => wrapper2 = shallow(<Window />));
  
    it('should render the Window Component', () => {
      expect(wrapper.containsMatchingElement(<Window />)).toEqual(true);
    });

    it('should render the LeftPanel and MainPanel Components', () => {
        expect(wrapper2.containsAllMatchingElements([
          <LeftPanel
            images={wrapper2.instance().state.images}
            allImages={wrapper2.instance().state.allImages}
            numberOfDraggedImages={wrapper2.instance().state.numberOfDraggedImages}
            showCollapseButton={wrapper2.instance().state.showCollapseButton}
            showPanel={wrapper2.instance().state.showPanel}
          />,
          <MainPanel
            imageDragged={wrapper2.instance().state.imageDragged}
            imageViewer={wrapper2.instance().state.imageViewer}
            imageStyles={wrapper2.instance().state.imageStyles}
            timesScrolledDown={wrapper2.instance().state.timesScrolledDown}
          />
        ])).toEqual(true);
    });
});